import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import site.clzblog.msshj.dao.IpDao;
import site.clzblog.msshj.entity.Ip;
import site.clzblog.msshj.service.IpService;

import javax.annotation.Resource;
import java.sql.Timestamp;

/**
 * @author chengli.zou
 * @date 21:09 2017/11/25
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-config.xml")
@Transactional
@TransactionConfiguration(transactionManager = "txManager")
public class Test {

    @Resource
    private IpService ipService;

    @Resource
    private IpDao ipDao;

    @org.junit.Test
    @Rollback(false)
    public void testPersistIp() {
        Ip ip = new Ip();
        ip.setIp("192.168.168.168");
        ip.setOpenId("clzblog666666");
        ip.setAddtime(new Timestamp(System.currentTimeMillis()));
        try {
            ipDao.persist(ip);
            System.out.println("Save Success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @org.junit.Test
    @Rollback(false)
    public void testSaveIp() {
        Ip ip = new Ip();
        ip.setIp("192.168.1.1");
        ip.setOpenId("clzblog");
        ip.setAddtime(new Timestamp(System.currentTimeMillis()));
        try {
            ipService.saveIp(ip);
            System.out.println("Save Success");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @org.junit.Test
    @Rollback(false)
    public void testDeleteIp() {
        Ip ip = new Ip();
        ip.setId(17);
        try {
            ipService.deleteIp(ip);
            System.out.println("Delete Success!");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    @org.junit.Test
    @Rollback(false)
    public void testUpdateIp() {
        Ip ip = new Ip();
        ip.setId(16);
        ip.setOpenId("chengli.zou@gmail.com");
        ip.setIp("192.168.100.100");
        ip.setAddtime(new Timestamp(System.currentTimeMillis()));
        try {
            ipService.updateIp(ip);
            System.out.println("Update Success!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @org.junit.Test
    @Rollback(false)
    public void testMergeIp() {
        Ip ip = new Ip();
        ip.setId(16);
        ip.setOpenId("chengli.zou");
        ip.setIp("192.168.100.168");
        ip.setAddtime(new Timestamp(System.currentTimeMillis()));
        try {
            ipService.mergeIp(ip);
            System.out.println("Merge Success!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @org.junit.Test
    @Rollback(false)
    public void testSaveOrUpdateIp() {
        Ip ip = new Ip();
        ip.setId(16);
        //ip.setOpenId("chengli.zou");
        ip.setIp("192.168.100.168");
        ip.setAddtime(new Timestamp(System.currentTimeMillis()));
        try {
            ipDao.saveOrUpdate(ip);
            System.out.println("Merge Success!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @org.junit.Test
    public void testFindById() {
        try {
            Ip ip = ipDao.findById(16);
            System.out.println("IpEntityInfo:" + ip);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @org.junit.Test
    public void testFindId() {
        try {
            Ip ip = ipDao.find(16);
            System.out.println("IpEntityInfo:" + ip);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @org.junit.Test
    @Rollback(false)
    public void testFindOneObj() {
        Ip ip = new Ip();
        ip.setIp("192.168.168.168");
        Object obj = ipDao.findOneObject("from Ip where ip=:ip", ip.getIp());
        Object obj1 = ipDao.findOneObject("from Ip where ip=:ip", ip.getIp());

        Ip obj2 = (Ip) obj;
        Ip obj3 = new Ip();
        BeanUtils.copyProperties(obj2, obj3);
        obj3.setId(188);
        ipDao.persist(obj3);
    }

}

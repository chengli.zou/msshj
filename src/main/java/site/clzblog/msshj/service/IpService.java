package site.clzblog.msshj.service;

import site.clzblog.msshj.entity.Ip;

/**
 * @author chengli.zou
 * @date 21:04 2017/11/25
 */
public interface IpService {

    void saveIp(Ip ip);

    void deleteIp(Ip ip);

    void updateIp(Ip ip);

    void mergeIp(Ip ip);

    Ip findById(int id);

}

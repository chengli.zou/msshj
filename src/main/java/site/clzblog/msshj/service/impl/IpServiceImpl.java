package site.clzblog.msshj.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import site.clzblog.msshj.dao.IpDao;
import site.clzblog.msshj.entity.Ip;
import site.clzblog.msshj.service.IpService;

import javax.annotation.Resource;

/**
 * @author chengli.zou
 * @date 21:06 2017/11/25
 */
@Service
@Transactional(propagation = Propagation.REQUIRED)
@TransactionConfiguration(transactionManager = "txManager")
public class IpServiceImpl implements IpService {
    @Resource
    private IpDao ipDao;

    public void saveIp(Ip ip) {
        ipDao.saveIp(ip);
    }

    public void deleteIp(Ip ip) {
        ipDao.deleteIp(ip);
    }

    public void updateIp(Ip ip) {
        ipDao.updateIp(ip);
    }

    public void mergeIp(Ip ip) {
        ipDao.mergeIp(ip);
    }

    public Ip findById(int id) {
        return ipDao.findById(id);
    }
}

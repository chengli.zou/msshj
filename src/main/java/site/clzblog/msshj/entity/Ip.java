package site.clzblog.msshj.entity;

import javax.persistence.*;
import java.sql.Timestamp;

/**
 * @author Administrator
 * @date 2018/2/2
 */
@Entity
public class Ip {
    private Integer id;
    private String openId;
    private String ip;
    private Timestamp addtime;

    @Id
    //@GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false)
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "openId", nullable = true, length = 30)
    public String getOpenId() {
        return openId;
    }

    public void setOpenId(String openId) {
        this.openId = openId;
    }

    @Basic
    @Column(name = "ip", nullable = true, length = 30)
    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip;
    }

    @Basic
    @Column(name = "addtime", nullable = true)
    public Timestamp getAddtime() {
        return addtime;
    }

    public void setAddtime(Timestamp addtime) {
        this.addtime = addtime;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ip ip1 = (Ip) o;

        if (id != null ? !id.equals(ip1.id) : ip1.id != null) return false;
        if (openId != null ? !openId.equals(ip1.openId) : ip1.openId != null) return false;
        if (ip != null ? !ip.equals(ip1.ip) : ip1.ip != null) return false;
        if (addtime != null ? !addtime.equals(ip1.addtime) : ip1.addtime != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (openId != null ? openId.hashCode() : 0);
        result = 31 * result + (ip != null ? ip.hashCode() : 0);
        result = 31 * result + (addtime != null ? addtime.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Ip{" +
                "id=" + id +
                ", openId='" + openId + '\'' +
                ", ip='" + ip + '\'' +
                ", addtime=" + addtime +
                '}';
    }
}

package site.clzblog.msshj.dao.base;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.util.Assert;

import javax.annotation.Resource;
import java.io.Serializable;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.List;

/**
 * @author chengli.zou
 * @date 20:19 2017/11/25
 */
public class BaseDao<T> {

    @Resource
    private SessionFactory sessionFactory;
    private Class<T> entityClass;

    public BaseDao() {
        //getGenericSuperclass() 通过反射获取当前类表示的实体（类，接口，基本类型或void）的直接父类的Type，getActualTypeArguments()返回参数数组。
        Type genType = getClass().getGenericSuperclass();
        Type[] param = ((ParameterizedType) genType).getActualTypeArguments();
        entityClass = (Class) param[0];
    }

    private Session getSession() {
        return sessionFactory.getCurrentSession() == null ? sessionFactory.openSession() : sessionFactory.getCurrentSession();
    }

    public T load(Serializable id) {
        return (T) getSession().load(entityClass, id);
    }

    public T get(Serializable id) {
        return (T) getSession().get(entityClass, id);
    }

    public void save(T entity) {
        getSession().save(entity);
    }

    public void persist(T entity) {
        getSession().persist(entity);
    }

    public void update(T entity) {
        getSession().update(entity);
    }

    public void delete(T entity) {
        getSession().delete(entity);
    }

    public void merge(T entity) {
        getSession().merge(entity);
    }

    public List getBySQLQuery(String sql) {
        Query query = getSession().createSQLQuery(sql);
        return query.list();
    }

    public Query createQuery(String hql, Object... values) {
        Assert.hasText(hql);
        Query query = getSession().createQuery(hql);
        for (int i = 0; i < values.length; i++) {
            query.setParameter(i, values[i]);
        }
        return query;
    }

    public void saveOrUpdate(T entity) {
        getSession().saveOrUpdate(entity);
    }

    public Object findOneObject(String hql, String param1) {
        return getSession().createQuery(hql).setParameter("ip", param1).uniqueResult();
    }

}

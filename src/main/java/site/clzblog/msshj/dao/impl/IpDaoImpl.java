package site.clzblog.msshj.dao.impl;

import org.hibernate.Query;
import org.springframework.stereotype.Repository;
import site.clzblog.msshj.dao.IpDao;
import site.clzblog.msshj.dao.base.BaseDao;
import site.clzblog.msshj.entity.Ip;

/**
 * @author chengli.zou
 * @date 20:59 2017/11/25
 */
@Repository
public class IpDaoImpl extends BaseDao<Ip> implements IpDao {
    public void saveIp(Ip ip) {
        super.save(ip);
    }

    public void deleteIp(Ip ip) {
        super.delete(ip);
    }

    public void updateIp(Ip ip) {
        super.update(ip);
    }

    public void mergeIp(Ip ip) {
        super.merge(ip);
    }

    public Ip findById(int id) {
        String hql = "from Ip ip where id =" + id;
        Query query = super.createQuery(hql);
        return (Ip) query.uniqueResult();
    }

    public Ip find(int id) {
        return super.load(id);
    }

    public Object findOneObject(String hql, String param) {
        return super.findOneObject(hql, param);
    }


}

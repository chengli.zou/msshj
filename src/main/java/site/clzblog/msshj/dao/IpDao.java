package site.clzblog.msshj.dao;

import site.clzblog.msshj.entity.Ip;

/**
 * @author chengli.zou
 * @date 20:57 2017/11/25
 */
public interface IpDao {

    void saveIp(Ip ip);

    void persist(Ip ip);

    void deleteIp(Ip ip);

    void updateIp(Ip ip);

    void mergeIp(Ip ip);

    Ip findById(int id);

    Ip find(int id);

    void saveOrUpdate(Ip ip);

    Object findOneObject(String hql, String param);
}
